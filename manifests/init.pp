class keepalived($service_ensure = running, $service_enable = true) {
    package {
        'ipvsadm':
            ensure => latest;
        'keepalived':
            ensure => latest;
    }
    service { 'ipvsadm':
        hasstatus => true,
        enable => $service_enable,
        ensure => $service_ensure
    }
    service { 'keepalived':
        require => Service[ 'ipvsadm' ],
        hasstatus => false,
        pattern => '/usr/sbin/keepalived',
        enable => $service_enable,
        ensure => $service_ensure
    }

    gem { 'drb':
            ensure => installed;
        'ipaddress':
            ensure => installed;
    }
    augeas { 'lvs_kernel_ops':
        context => "/files/etc/sysctl.conf",
        changes => [
            "set net.ipv4.ip_nonlocal_bind 1"
        ],
        notify => Exec[ 'load_lvs_kernel_ops' ]
    }
    exec { "/sbin/sysctl -p":
        alias => 'load_lvs_kernel_ops',
        subscribe => Augeas[ 'lvs_kernel_ops' ],
        refreshonly => true
    }
    
    define global_defs(
        $notification_email,
        $notification_email_from,
        $smtp_server = '127.0.0.1',
        $smtp_connect_timeout = 30,
        $router_id = $title
    ) {
        include concat::setup
        concat{ '/etc/keepalived/keepalived.conf':
            notify => Service[ 'keepalived' ]
        }
        concat::fragment{ "frag_header_$title":
            target => '/etc/keepalived/keepalived.conf',
            order => 00,
            content => "# Managed by puppet module 'keepalived'. Do not edit manually\n\n"
        }
        concat::fragment{ "frag_global_defs_$title":
            target => '/etc/keepalived/keepalived.conf',
            order => 10,
            content => template('keepalived/global_defs.erb')
        }
    }

    define vrrp_script(
        $script,
        $interval = false,
        $weight = false
    ) {
        concat::fragment{ "frag_vrrp_script_$title":
            target => '/etc/keepalived/keepalived.conf',
            order => 20,
            content => template('keepalived/vrrp_script.erb')
        }
    }

    define vrrp_instance(
        $state,
        $interface,
        $garp_master_delay = 0,
        $smtp_alert = false,
        $virtual_router_id,
        $priority = 0,
        $advert_int = 0,
        $auth_type = false,
        $auth_pass = false,
        $virtual_ipaddress,
        $track_script = false
    ) {
        concat::fragment{ "frag_vrrp_instance_$title":
            target => '/etc/keepalived/keepalived.conf',
            order => 30,
            content => template('keepalived/vrrp_instance.erb')
        }
    }

    define virtual_server(
        $status_code = 200,
        $bindto = false,
        $digest = false,
        $inhibit_on_failure = true,
        $weight = 1,
        $port,
        $connect_timeout = false,
        $nb_get_retry = false,
        $delay_before_retry = false,
        $connect_port = $port,
        $delay_loop = 6,
        $lb_algo = 'rr',
        $lb_kind = 'DR',
        $persistence_timeout = 0,
        $protocol = 'TCP',
        $virtualhost,
        $real_server,
        $uripath = '/',
        $service_check
    ) {
        concat::fragment{ "frag_virtual_server_$title":
            target => '/etc/keepalived/keepalived.conf',
            order => 40,
            content => template('keepalived/virtual_server.erb')
        }
    }
}

class keepalived::backend {
    augeas { 'disable_invalid_arps':
        context => "/files/etc/sysctl.conf",
        changes => [
            "set net.ipv4.conf.all.arp_ignore 1",
            "set net.ipv4.conf.all.arp_announce 2"
        ]
    }
    exec { "/sbin/sysctl -p":
        alias => 'load_kernel_ops',
        subscribe => Augeas[ 'disable_invalid_arps' ],
        refreshonly => true
    }
    define vip(
        $vip,
        $device
    ) { 
        augeas { "keepalived-backend-$device":
            require => [ Augeas[ 'disable_invalid_arps' ], Exec[ 'load_kernel_ops' ] ],
            context => "/files/etc/network/interfaces",
            changes => [
                "set allow-hotplug/1 lo",
                "set allow-hotplug/2 eth0",
                "set iface[. = '$device'] $device",
                "set iface[. = '$device']/family inet",
                "set iface[. = '$device']/method static",
                "set iface[. = '$device']/address $vip",
                "set iface[. = '$device']/netmask 255.255.255.255"
            ]
        }
        exec {"/sbin/ifup $device":
            require => Augeas[ "keepalived-backend-$device" ],
            command => "/sbin/ifup $device",
            unless  => "/sbin/ifconfig | grep $device"
        }
    }
}
